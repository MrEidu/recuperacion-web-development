const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _first_name: String,
    _last_name: String,
    _email: String,
    _phone_number: Number,
    _hire_date: Date,
    _job_id: mongoose.Types.ObjectId,
    _salary: Number,
    _commission_pct: Number,
    _manager_id : mongoose.Types.ObjectId,
    _department_id: mongoose.Types.ObjectId
});
class Employee{

    constructor(first_name,last_name,email,phone_number,hire_date,job_id,salary,commission_pct,manager_id,department_id) {
        this._first_name= first_name
        this._last_name=last_name
        this._email=email
        this._phone_number=phone_number
        this._hire_date=hire_date
        this._job_id=job_id
        this._salary=salary
        this._commission_pct=commission_pct
        this._manager_id=manager_id
        this._department_id=department_id
    }

    get first_name() {
        return this._first_name;
    }

    set first_name(v) {
        this._first_name = v;
    }

    get last_name() {
        return this._last_name;
    }

    set last_name(v) {
        this._last_name = v;
    }

    get email() {
        return this._email;
    }

    set email(v) {
        this._email = v;
    }
    
    get phone_number() {
        return this._phone_number;
    }

    set phone_number(v) {
        this._phone_number = v;
    }
    
    get hire_date() {
        return this._hire_date;
    }

    set hire_date(v) {
        this._hire_date = v;
    }
    
    get job_id() {
        return this._job_id;
    }

    set job_id(v) {
        this._job_id = v;
    }
    
    get salary() {
        return this._salary;
    }

    set salary(v) {
        this._salary = v;
    }
    
    get commission_pct() {
        return this._commission_pct;
    }

    set commission_pct(v) {
        this._commission_pct = v;
    }
    
    get manager_id() {
        return this._manager_id;
    }

    set manager_id(v) {
        this._manager_id = v;
    }

    get department_id() {
        return this._department_id;
    }

    set department_id(v) {
        this._department_id = v;
    }

}

schema.loadClass(Employee);
module.exports = mongoose.model('Employe', schema);