const async = require('async');
const bcrypt = require('bcrypt');
const { json } = require('express');
const Employee = require('../models/employee');

function list(req, res, next) {
  const page = req.query.page ? req.query.page : 1;
  Employee.paginate({

  }, {page: page, limit: 8}).then(employee => res.status(200).json({
      message: res.__('employee.list.ok'),
      objs: employee
  })).catch(error => res.status(500).json({
      message: res.__('employee.list.err'),
      obj: error
  }));
  }
  
  function index(req, res) {
    let id = req.params.id;
    Employee.findOne({_id: id}).then(employee => res.status(200).json({
        message: res.__('employee.index.ok'),
        objs: employee
    })).catch(error => res.status(500).json({
        message: res.__('employee.index.err'),
        obj: error
    }));
  }
  
  function create(req, res) {

    let first_name = req.body.first_name
    let last_name = req.body.last_name
    let email = req.body.email
    let phone_number = req.body.phone_number
    let hire_date = req.body.hire_date
    let job_id = req.body.job_id
    let salary = req.body.salary
    let commission_pct = req.body.commission_pct
    let manager_id = req.body.manager_id
    let department_id = req.body.department_id

    let employee = new Employee({
      _first_name: first_name,
      _last_name:last_name,
      _email:email,
      _phone_number:phone_number,
      _hire_date:hire_date,
      _job_id:job_id,
      _salary:salary,
      _commission_pct:commission_pct,
      _manager_id:manager_id,
      _department_id:department_id
    });

    employee.save().then(employee => res.status(200).json({
      message : res.__('employee.create.ok'),
      objs : employee
    })).catch(err=> res.status(500).json({
      message : res.__('employee.create.err'),
      objs : err
    }));

  }
  
  function update(req, res) {
    let id = req.params.id;

    let employee = new Object();

    if(req.body.first_name)
      employee._first_name = req.body.first_name;
    if(req.body.last_name)
      employee._last_name = req.body.last_name;
    if(req.body.email)
      employee._email = req.body.email;
    if(req.body.phone_number)
      employee._phone_number = req.body.phone_number;
    if(req.body.hire_date)
      employee._hire_date = req.body.hire_date;
    if(req.body.hob_id)
      employee._hob_id = req.body.hob_id;
    if(req.body.commission_pct)
      employee._commission_pct = req.body.commission_pct;
    if(req.body.manager_id)
      employee._manager_id = req.body.manager_id;
    if(req.body.department_id)
      employee._department_id = req.body.department_id;


    Employee.findOneAndUpdate({_id: id}, employee, {omitUndefined: true}).then(employee => res.status(200).json({
        message: res.__('employee.update.ok'),
        objs: employee
    })).catch(error => res.status(500).json({
        message: res.__('employee.update.err'),
        obj: error
    }));
  }
  
  function destroy(req, res) {
    const id = req.params.id;
    Employee.deleteOne({_id: id}).then(employee => res.status(200).json({
        message: res.__('employee.destroy.ok'),
        objs: employee
    })).catch(error => res.status(500).json({
        message: res.__('employee.destroy.err'),
        obj: error
    }));
  }
  
  module.exports = {
    list,
    create,
    update,
    destroy,
    index
  }